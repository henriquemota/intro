//
//  main.cpp
//  estrutura
//
//  Created by Henrique Mota on 05/05/19.
//  Copyright © 2019 Henrique Mota. All rights reserved.
//

#include <iostream>

using namespace std;

struct pessoa {
    string nome;
    int idade;
};

int main (void){
    pessoa p;
    p.nome = "joao";
    p.idade = 44;
    cout << "Olá " << p.nome << ", você tem " << p.idade << " anos" << endl;
    return EXIT_SUCCESS;
}
