//
//  funcoes.cpp
//  repeticao-while
//
//  Created by Henrique Mota on 15/05/19.
//  Copyright © 2019 Henrique Mota. All rights reserved.
//

#include <iostream>
#include <unistd.h>

using namespace std;

void calcularMediaIdades() {
    int m = 0, f = 0, t = 0, countm = 0, countf = 0;
    char sexo, parada = 'n';
    
    while (parada == 'n') {
        cout << "Informe o sexo ('m' ou 'f'): " << endl;
        cin >> sexo;
        
        cout << "Informe a idade: " << endl;
        cin >> t;
        
        if (sexo == 'm') {
            m += t;
            countm++;
        }
        else {
            f += t;
            countf++;
        }
        
        cout << "Deseja encerrar o programa ('s' ou 'n')? " << endl;
        cin >> parada;
        
    }
    
    cout << "Média das idades dos " << countm << " homens é " << m / countm << " anos " << endl;
    cout << "Média das idades das " << countf << " mulheres é " << f / countf << " anos " << endl;
    cout << "Média total das " << countf + countm << " pessoas é " << (f + m) / (countf + countm) << " anos " << endl;
}

void calcularParcelas () {
    
    double valorOriginal, valorPrincipal, valorFinal, taxa;
    int parcelas, i;
    
    cout << "Informe o valor desejado: " << endl;
    cin >> valorOriginal;
    
    cout << "Informe a quantidade de parcelas: " << endl;
    cin >> parcelas;
    
    cout << "Informe a taxa mensal: " << endl;
    cin >> taxa;
    
    i = 1;
    valorPrincipal = valorOriginal / parcelas; // calcula o valor do principal
    valorFinal = 0;
    
    double valorJuros = (valorPrincipal * taxa) / 100;
    
    while(i <= parcelas) {
        cout << "Valor da parcela " << i << " de " << parcelas << " => " << valorJuros + valorPrincipal << endl;
        valorFinal += valorJuros + valorPrincipal ;
        i++;
    }
    
    cout << "O valor final financiado é " << valorFinal << endl;
}


/*
 Desenvolva um Programa em C++ usando o comando DO – WHILE para ler os dados (sexo e média) de n alunos.
 Apresentar: A média do grupo; A média das mulheres e dos homens; O Percentual de Homens e Mulheres na turma
 
 mg = (sidm + sidh) / total
 mm = sidm / totalm
 mh = sidh / totalh
 ph = totalh * 100 / total
 pm = totalm * 100 / total
*/

void calcularPercentualHomensMulheres() {
    int mg, mm, mh, somah = 0, somam = 0, idadeh = 0, idadem = 0, totalh = 0, totalm = 0;
    double ph, pm;
    char condicao = 's', sexo;
    
    do {
        cout << "informe o sexo (m: mulher, h: homem): ";
        cin >> sexo;
        cout << "informe a idade: ";
        if (sexo == 'm') {
            cin >> idadem;
            somam += idadem;
            totalm++;
        }
        else if (sexo == 'h') {
            cin >> idadeh;
            somah += idadeh;
            totalh++;
        }
        
        cout << "deseja informar mais outro valor (s: sim, n: nao): ";
        cin >> condicao;

    } while (condicao == 's');
    
    mg = (somah + somam) / (totalh + totalm);
    mm = somam / totalm;
    mh = somah / totalh;
    ph = totalh * 100 / (totalh + totalm);
    pm = totalm * 100 / (totalh + totalm);
    
    cout << "a media global e " << mg << endl;
    cout << "a media das mulheres e " << mm << endl;
    cout << "a media dos homens e " << mh << endl;
    cout << "o percentual de mulheres e " << pm << endl;
    cout << "o percentual de homens e " << ph << endl;
}

/*
Desenvolva um Programa em C++ usando o comando DO - WHILE,
para apresentar os valores que um relógio digital assume nos dígitos HH:MM:SS.
*/
void relogioDigital() {
    
    int h = 0, m = 0, s = 0;
    string sh, sm, ss;
    do {
        
        sh = (h < 10) ?  "0" + to_string(h) : to_string(h);
        sm = (m < 10) ?  "0" + to_string(m) : to_string(m);
        ss = (s < 10) ?  "0" + to_string(s) : to_string(s);

        cout << sh << ":" << sm << ":" << ss << endl;
        s++;
        if (s == 60) {
            m++;
            s = 0;
        }
        if (m == 60) {
            h++;
            m = 0;
        }
        //usleep(60000); // segundo a segundo
    } while (h < 24);
}

void calcularValorPagamento() {
    
    /*
        Faça o algoritmo para calcular quanto será pago por um produto (PAG),
        sendo que o preço do produto (PR) e o desconto (D), em porcentagem,
        são fornecidos pelo usuário. Apresentar o valor a ser pago pelo produto.
     */
    
    float pag = 0, pr = 0, d = 0, valorDesconto = 0;
    
    cout << "informe o valor do produto: " << endl;
    cin >> pr;

    cout << "informe o valor do desconto: " << endl;
    cin >> d;
    
    valorDesconto = (pr * d) / 100;
    pag = pr - valorDesconto;
    
    cout << "o valor devido é " << pag << endl;
}
