//
//  main.cpp
//  repeticao
//
//  Created by Henrique Mota on 05/05/19.
//  Copyright © 2019 Henrique Mota. All rights reserved.
//

#include <iostream>

using namespace std;

int fatorial1(int x){
    if (x == 0 || x == 1)
        return 1;
    else
        return (x * fatorial1(x - 1));
}

int fatorial2(int x) {
    int fat = x;
    for (; x > 1; x--)
        fat = fat * (x - 1);
    return fat;
}

void pares(int num) {
    int soma = 0;
    for (int i = 0; i <= num; i++) {
        if (i % 2 == 0) {
            soma += i;
            cout << "Número " << i << " é par. Soma = " << soma << endl;
        }
    }
    cout << "Soma total = " << soma << endl;
}

void pares() {
    int soma = 0;
    for (int i = 0; i <= 100; i += 2) {
        soma += i;
        cout << "Número " << i << " é par. Soma = " << soma << endl;
    }
    cout << "Soma total = " << soma << endl;
}

void pares2() {
    int soma = 0;
    int x = 0;
    while (x <= 100) {
        soma += x;
        cout << "Número " << x << " é par. Soma = " << soma << endl;
    }
    cout << "Soma total = " << soma << endl;
}

void multiplicar(int num) {
    for (int i = 1; i <= 10; i++) {
        cout << num << " x " << i << " = " << num * i << endl;
    }
}

int main(void) {
    return EXIT_SUCCESS;
}
